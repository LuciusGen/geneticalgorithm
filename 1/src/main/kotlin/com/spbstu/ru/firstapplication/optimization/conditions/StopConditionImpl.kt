package com.spbstu.ru.firstapplication.optimization.conditions

import com.spbstu.ru.firstapplication.optimization.Individual
import com.spbstu.ru.firstapplication.optimization.Population
import kotlinx.coroutines.DelicateCoroutinesApi
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class StopConditionImpl(
    private val sameValueMaxIterations: Int = 50,
) : StopCondition {
    private var prevBestIndividual: Individual? = null
    private var currentSameValueIterations = 0
    private var currentIteration = 0

    private val logger = LoggerFactory.getLogger(StopConditionImpl::class.java)

    @DelicateCoroutinesApi
    override fun checkStopCondition(population: Population, maxIterations: Int): Boolean {
        val bestIndividual = population.getBestIndividualInPopulation().individual
        currentIteration += 1
        logger.info("start iteration number $currentIteration")

        if (prevBestIndividual == null || prevBestIndividual != bestIndividual) {
            prevBestIndividual = bestIndividual
            currentSameValueIterations = 0
        }

        if (prevBestIndividual == bestIndividual) {
            currentSameValueIterations += 1
        }

        if (currentIteration == maxIterations || currentSameValueIterations == sameValueMaxIterations)
            return true

        return false
    }
}