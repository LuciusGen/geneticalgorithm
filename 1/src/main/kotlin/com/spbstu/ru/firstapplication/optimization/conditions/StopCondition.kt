package com.spbstu.ru.firstapplication.optimization.conditions

import com.spbstu.ru.firstapplication.optimization.Population
import kotlinx.coroutines.DelicateCoroutinesApi

interface StopCondition {
    @DelicateCoroutinesApi
    fun checkStopCondition(population: Population, maxIterations: Int): Boolean
}