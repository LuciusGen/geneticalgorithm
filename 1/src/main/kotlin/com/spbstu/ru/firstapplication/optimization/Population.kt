package com.spbstu.ru.firstapplication.optimization

import com.spbstu.ru.firstapplication.input.InputParams
import com.spbstu.ru.firstapplication.optimization.function.CellFunction
import com.spbstu.ru.firstapplication.optimization.function.CellFunctionComputer
import kotlinx.coroutines.DelicateCoroutinesApi
import org.slf4j.LoggerFactory

data class IndividualInPopulation(
    val individual: Individual,
    val value: Double
)

@DelicateCoroutinesApi
class Population(
    private val configuration: Configuration,

    private val cellFunctionComputer: CellFunctionComputer,
    private val cellFunction: CellFunction,

    populationSize: Int = 300,
    private val params: InputParams
) {
    internal val individualsWithValue: MutableMap<Individual, Double>
    private val logger = LoggerFactory.getLogger(Population::javaClass.name)

    init {
        individualsWithValue = HashMap(populationSize)

        val nullIndividuals = configuration.initializationStrategy.initialize(
            populationSize,
            params.cellFunctionParamsNumber,
            params.limit
        )
        val individualsCellFunctionValues = cellFunctionComputer.compute(cellFunction, nullIndividuals)

        putIndividualsToMap(nullIndividuals, individualsCellFunctionValues)
    }

    fun setNewPopulation(newIndividuals: List<Individual>) {
        val notUniqueData = individualsWithValue.filter { it.key in newIndividuals }
        val unique = individualsWithValue.filter { it.key !in newIndividuals }.keys.toList()

        val uniqueIndividualsCellFunctionValues = cellFunctionComputer.compute(cellFunction, unique)
        this.individualsWithValue.clear()

        putIndividualsToMap(unique, uniqueIndividualsCellFunctionValues)
        this.individualsWithValue.putAll(notUniqueData)
        logger.info("Set new population")
    }

    fun crossoverPopulation(): Map<Individual, Double> {
        val newIndividuals = ArrayList<Individual>()

        for (i in individualsWithValue) {
            val firstIndividual = configuration.selectionStrategy.select(this)
            val secondIndividual = configuration.selectionStrategy.select(this)
            newIndividuals.add(configuration.crossoverStrategy.crossover(firstIndividual, secondIndividual))
        }
        logger.info("Crossover population")
        setNewPopulation(newIndividuals)
        return this.individualsWithValue
    }

    fun getBestIndividualInPopulation(tournament: List<Individual> = this.individualsWithValue.keys.toList()): IndividualInPopulation {
        var fittest = Individual(params.cellFunctionParamsNumber)
        var minValue = Double.MAX_VALUE

        for (individual in tournament) {
            val value = this.individualsWithValue[individual] ?: Double.MAX_VALUE

            if (minValue > value) {
                fittest = individual
                minValue = value
            }
        }

        return IndividualInPopulation(
            fittest,
            minValue
        )
    }

    override fun toString(): String {
        return individualsWithValue.keys.joinToString(separator = " ; ")
    }

    private fun putIndividualsToMap(
        newIndividuals: List<Individual>,
        individualsCellFunctionValues: List<Double>
    ) {
        for (i in newIndividuals.indices)
            individualsWithValue[newIndividuals[i]] = individualsCellFunctionValues[i]
    }
}