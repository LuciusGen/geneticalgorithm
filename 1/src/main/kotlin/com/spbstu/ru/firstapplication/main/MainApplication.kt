package com.spbstu.ru.firstapplication.main

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan(
    "com.spbstu.ru.firstapplication"
)
class MainApplication

fun main(args: Array<String>) {
    runApplication<MainApplication>(*args)
}
