package com.spbstu.ru.firstapplication.optimization

import com.spbstu.ru.firstapplication.input.InputParams
import com.spbstu.ru.firstapplication.optimization.conditions.StopCondition
import com.spbstu.ru.firstapplication.optimization.mutation.Mutation
import kotlinx.coroutines.DelicateCoroutinesApi
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

data class OptimizationResult(
    val params: List<Double>,
    val cellFunctionValue: Double
)

@Service
@DelicateCoroutinesApi
class GeneticAlgorithmOptimization(
    private val backUpper: BackUpper,
    private val stopCondition: StopCondition
) {
    private val logger = LoggerFactory.getLogger(Population::javaClass.name)

    fun optimize(
        params: InputParams,
        population: Population,
        mutationStrategy: Mutation
    ): OptimizationResult {
        val kj = (0 until params.cellFunctionParamsNumber).map { params.kj }
        val zj = (0 until params.cellFunctionParamsNumber).map { params.zj }

        while (!stopCondition.checkStopCondition(population, params.maxIter)) {
            val crossoverIndividuals = population.crossoverPopulation()

            logger.info("Mutate population")
            val mutationIndividuals = crossoverIndividuals.map {
                mutationStrategy.mutate(
                    individual = it.key, limit = params.limit,
                    value = it.value, kj = kj, zj = zj
                )
            }

            population.setNewPopulation(mutationIndividuals)

            backUpper.backUp(population, params.backUpIter)
        }

        val bestIndividual = population.getBestIndividualInPopulation()

        return OptimizationResult(
            bestIndividual.individual.cellFunctionParams,
            bestIndividual.value
        )
    }
}