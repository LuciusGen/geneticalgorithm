package com.spbstu.ru.firstapplication.optimization.selection

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration

@Configuration
class SelectionStrategyConfiguration(
    private val defaultSelection: SelectionTournament = SelectionTournament()
) {
    private val selectionStrategies = HashMap<String, Selection>(10)
    private val logger = LoggerFactory.getLogger(SelectionStrategyConfiguration::class.java)
    init {
        selectionStrategies["default"] = defaultSelection
    }

    fun getSelectionStrategy(strategyName: String): Selection {
        val strategy = selectionStrategies.getOrDefault(strategyName, defaultSelection)
        logger.info("Create selection strategy with name ${strategy::class.java}")
        return strategy
    }
}