package com.spbstu.ru.firstapplication.optimization.crossover

import com.spbstu.ru.firstapplication.optimization.Individual

class DefaultCrossoverStrategy(
    private val uniformRate: Double = 0.8
) : Crossover {
    override fun crossover(first: Individual, second: Individual): Individual {
        val newIndividual = Individual(first.cellFunctionParams.size)

        for (i in 0 until newIndividual.cellFunctionParams.size) {
            newIndividual.cellFunctionParams[i] =
                if (Math.random() <= uniformRate) first.cellFunctionParams[i] else second.cellFunctionParams[i]
        }

        return newIndividual
    }
}