package com.spbstu.ru.firstapplication.optimization.initialization

import com.spbstu.ru.firstapplication.optimization.Individual
import net.jafama.FastMath

class InitializationDefault : Initialization {
    override fun initialize(size: Int, individualParamsNumber: Int, limit: Double): List<Individual> {
        val individuals = ArrayList<Individual>(size)

        repeat(size) {
            individuals.add(generateRandomIndividual(individualParamsNumber, limit))
        }
        return individuals
    }

    private fun generateRandomIndividual(paramsNumber: Int, limit: Double): Individual {
        val individual = Individual(paramsNumber)

        for (i in individual.cellFunctionParams.indices)
            individual.cellFunctionParams[i] = (FastMath.random() * 2 - 1) * limit

        return individual
    }
}