package com.spbstu.ru.firstapplication.optimization.function

import com.spbstu.ru.firstapplication.optimization.Individual
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import java.util.concurrent.Executors

@Service
@DelicateCoroutinesApi
class CellFunctionComputer {
    private val executor = Executors.newFixedThreadPool(6).asCoroutineDispatcher()

    @Suppress("UNCHECKED_CAST")
    fun compute(cellFunction: CellFunction, individuals: List<Individual>): List<Double> = runBlocking {
        individuals.map { async(executor) { cellFunction.compute(it) } }.map { it.await() }
    }
}