package com.spbstu.ru.firstapplication.optimization.mutation

import com.spbstu.ru.firstapplication.optimization.Individual
import net.jafama.FastMath

class DefaultMutationStrategy(
    private val mutationRate: Double = 0.05
) : Mutation {
    override fun mutate(
        individual: Individual,
        limit: Double,
        value: Double,
        kj: List<Double>,
        zj: List<Double>
    ): Individual {
        val mutateIndividual = Individual(individual.cellFunctionParams.size)

        for (i in 0 until individual.cellFunctionParams.size) {
            if (FastMath.random() <= mutationRate) {
                val newIValue =
                    individual.cellFunctionParams[i] + FastMath.sqrt(kj[i] * value + zj[i]) * FastMath.random()
                mutateIndividual.cellFunctionParams[i] =
                    if (newIValue in (-limit..limit)) newIValue else individual.cellFunctionParams[i]
            } else {
                mutateIndividual.cellFunctionParams[i] = individual.cellFunctionParams[i]
            }
        }

        return mutateIndividual
    }
}