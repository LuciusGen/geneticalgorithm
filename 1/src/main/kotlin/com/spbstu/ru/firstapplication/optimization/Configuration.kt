package com.spbstu.ru.firstapplication.optimization

import com.spbstu.ru.firstapplication.optimization.crossover.Crossover
import com.spbstu.ru.firstapplication.optimization.initialization.Initialization
import com.spbstu.ru.firstapplication.optimization.mutation.Mutation
import com.spbstu.ru.firstapplication.optimization.selection.Selection

data class Configuration(
    val crossoverStrategy: Crossover,
    val selectionStrategy: Selection,
    val initializationStrategy: Initialization,
    val mutationStrategy: Mutation
)