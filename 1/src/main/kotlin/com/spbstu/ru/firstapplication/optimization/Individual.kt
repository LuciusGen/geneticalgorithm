package com.spbstu.ru.firstapplication.optimization


class Individual(
    paramsNumber: Int
) {
    val cellFunctionParams = MutableList(paramsNumber) { 0.0 }

    override fun toString(): String {
        return cellFunctionParams.joinToString(separator = " , ", prefix = "[", postfix = "]")
    }

    override fun equals(other: Any?): Boolean {
        if (other == null || other::class.java != this::class.java)
            return false

        return this.cellFunctionParams == (other as Individual).cellFunctionParams
    }
}
