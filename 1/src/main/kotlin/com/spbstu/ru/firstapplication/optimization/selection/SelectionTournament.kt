package com.spbstu.ru.firstapplication.optimization.selection

import com.spbstu.ru.firstapplication.optimization.Individual
import com.spbstu.ru.firstapplication.optimization.Population
import kotlinx.coroutines.DelicateCoroutinesApi
import net.jafama.FastMath

class SelectionTournament(
    private val tournamentSize: Int = 50
) : Selection {
    @DelicateCoroutinesApi
    override fun select(population: Population): Individual {
        val tournament = ArrayList<Individual>(population.individualsWithValue.size)
        val individualList = population.individualsWithValue.keys.toList()

        repeat(tournamentSize) {
            val randId = (FastMath.random() * population.individualsWithValue.keys.size).toInt()

            tournament.add(individualList[randId])
        }

        return population.getBestIndividualInPopulation(tournament).individual
    }
}