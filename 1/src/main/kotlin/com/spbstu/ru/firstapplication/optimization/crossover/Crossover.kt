package com.spbstu.ru.firstapplication.optimization.crossover

import com.spbstu.ru.firstapplication.optimization.Individual

interface Crossover {
    fun crossover(first: Individual, second: Individual): Individual
}