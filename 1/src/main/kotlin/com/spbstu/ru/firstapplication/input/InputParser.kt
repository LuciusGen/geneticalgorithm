package com.spbstu.ru.firstapplication.input

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.io.File

@Service
class InputParser {
    private val logger = LoggerFactory.getLogger(InputParser::class.java)
    private val paramsMap = HashMap<String, String?>()

    init {
        paramsMap["cellFunction"] = null
        paramsMap["params"] = null
        paramsMap["limit"] = null
        paramsMap["kj"] = null
        paramsMap["zj"] = null
        paramsMap["backupIter"] = "100"
        paramsMap["maxIter"] = "100"
    }

    fun parseInput(inputFileName: String): InputParams? {
        val file = File(inputFileName)

        try {
            val lines = file.readLines()
            logger.info("Read lines from file")
            return parseLines(lines)
        } catch (exception: Exception) {
            System.err.println("Error with input file")
        }

        return null
    }

    private fun parseLines(lines: List<String>): InputParams? {
        try {
            lines.forEach {
                val parseLine = it.split("=").toTypedArray()

                if (parseLine[0] in paramsMap.keys)
                    paramsMap[parseLine[0]] = parseLine[1]
            }
        } catch (exception: Exception) {
            System.err.println("Error with file reading")
        }

        return convertToData()
    }

    private fun convertToData(): InputParams? {
        if (paramsMap.values.filterNotNull().size != paramsMap.size)
            return null
        logger.info("Lines have been parsed")
        return InputParams(
            paramsMap["cellFunction"]!!,
            paramsMap["params"]!!.toInt(),
            paramsMap["limit"]!!.toDouble(),
            paramsMap["kj"]!!.toDouble(),
            paramsMap["zj"]!!.toDouble(),
            paramsMap["backupIter"]!!.toInt(),
            paramsMap["maxIter"]!!.toInt()
        )
    }
}