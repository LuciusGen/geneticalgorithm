package com.spbstu.ru.firstapplication.optimization.crossover

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration

@Configuration
class CrossoverStrategyConfiguration(
    private val defaultCrossoverStrategy: DefaultCrossoverStrategy = DefaultCrossoverStrategy()
) {
    private val crossoverStrategies = HashMap<String, Crossover>(10)
    private val logger = LoggerFactory.getLogger(CrossoverStrategyConfiguration::class.java)

    init {
        crossoverStrategies["default"] = defaultCrossoverStrategy
    }

    fun getCrossoverStrategy(strategyName: String): Crossover {
        val strategy = crossoverStrategies.getOrDefault(strategyName, defaultCrossoverStrategy)
        logger.info("Create crossover strategy with name ${strategy::class.java}")
        return strategy
    }
}