package com.spbstu.ru.firstapplication.optimization.initialization

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration

@Configuration
class InitializationStrategyConfiguration(
    private val initializationDefault: InitializationDefault = InitializationDefault()
) {
    private val initializeStrategies = HashMap<String, Initialization>(10)
    private val logger = LoggerFactory.getLogger(InitializationStrategyConfiguration::class.java)

    init {
        initializeStrategies["default"] = initializationDefault
    }

    fun getInitializationStrategy(strategyName: String): Initialization {
        val strategy = initializeStrategies.getOrDefault(strategyName, initializationDefault)
        logger.info("Create initialization strategy with name ${strategy::class.java}")
        return strategy
    }
}