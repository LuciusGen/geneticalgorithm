package com.spbstu.ru.firstapplication.optimization.selection

import com.spbstu.ru.firstapplication.optimization.Individual
import com.spbstu.ru.firstapplication.optimization.Population
import kotlinx.coroutines.DelicateCoroutinesApi

interface Selection {
    @DelicateCoroutinesApi
    fun select(population: Population): Individual
}