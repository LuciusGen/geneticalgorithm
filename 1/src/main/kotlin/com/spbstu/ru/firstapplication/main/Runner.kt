package com.spbstu.ru.firstapplication.main

import com.spbstu.ru.firstapplication.input.InputParams
import com.spbstu.ru.firstapplication.input.InputParser
import com.spbstu.ru.firstapplication.optimization.Configuration
import com.spbstu.ru.firstapplication.optimization.GeneticAlgorithmOptimization
import com.spbstu.ru.firstapplication.optimization.OptimizationResult
import com.spbstu.ru.firstapplication.optimization.Population
import com.spbstu.ru.firstapplication.optimization.crossover.CrossoverStrategyConfiguration
import com.spbstu.ru.firstapplication.optimization.function.CellFunction
import com.spbstu.ru.firstapplication.optimization.function.CellFunctionComputer
import com.spbstu.ru.firstapplication.optimization.initialization.InitializationStrategyConfiguration
import com.spbstu.ru.firstapplication.optimization.mutation.Mutation
import com.spbstu.ru.firstapplication.optimization.mutation.MutationStrategyConfiguration
import com.spbstu.ru.firstapplication.optimization.selection.SelectionStrategyConfiguration
import kotlinx.coroutines.DelicateCoroutinesApi
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.stereotype.Component
import java.io.File
import kotlin.system.exitProcess

@Component
@DelicateCoroutinesApi
class Runner : CommandLineRunner {
    private val outFileName = "out.txt"
    private val logger = LoggerFactory.getLogger(Runner::class.java)

    @Autowired
    lateinit var applicationContext: ConfigurableApplicationContext

    override fun run(vararg args: String?) {
        if (args.isEmpty()) {
            System.err.println("You should write file name as command line argument")
            return
        }
        val fileName = args[0]!!

        val inputParser = applicationContext.getBean(InputParser::class.java)
        val inputParams = inputParser.parseInput(inputFileName = fileName) ?: return

        val configuration = getConfigurations()

        val population = initPopulation(inputParams, configuration)
        val optimizationResult = runOptimization(population, configuration.mutationStrategy, inputParams)

        writeResult(optimizationResult)

        applicationContext.close()
        exitProcess(0)
    }

    private fun initPopulation(inputParams: InputParams, configuration: Configuration): Population {
        val cellFunctionComputer = applicationContext.getBean(CellFunctionComputer::class.java)
        val cellFunction = CellFunction(inputParams.cellFunctionName)

        return Population(
            configuration = configuration,
            cellFunctionComputer = cellFunctionComputer,
            cellFunction = cellFunction,
            params = inputParams
        )
    }

    private fun getConfigurations(): Configuration {
        val mutationChooser = applicationContext.getBean(MutationStrategyConfiguration::class.java)
        val crossoverChooser = applicationContext.getBean(CrossoverStrategyConfiguration::class.java)
        val selectionChooser = applicationContext.getBean(SelectionStrategyConfiguration::class.java)
        val initializationChooser = applicationContext.getBean(InitializationStrategyConfiguration::class.java)

        return Configuration(
            mutationStrategy = mutationChooser.getMutationStrategy(""),
            crossoverStrategy = crossoverChooser.getCrossoverStrategy(""),
            selectionStrategy = selectionChooser.getSelectionStrategy(""),
            initializationStrategy = initializationChooser.getInitializationStrategy("")
        )
    }

    private fun runOptimization(
        population: Population,
        mutationStrategy: Mutation,
        inputParams: InputParams
    ): OptimizationResult {
        val optimizer = applicationContext.getBean(GeneticAlgorithmOptimization::class.java)
        logger.info("Run optimization")

        return optimizer.optimize(
            params = inputParams,
            population = population,
            mutationStrategy = mutationStrategy
        )
    }

    private fun writeResult(optimizationResult: OptimizationResult) {
        logger.info("Write result")
        File(outFileName).printWriter().use { out ->
            out.println("params: ${optimizationResult.params.joinToString(separator = ",")}")
            out.println("cell function value: ${optimizationResult.cellFunctionValue}")
        }
    }
}