package com.spbstu.ru.firstapplication.optimization.initialization

import com.spbstu.ru.firstapplication.optimization.Individual

interface Initialization {
    fun initialize(size: Int, individualParamsNumber: Int, limit: Double): List<Individual>
}