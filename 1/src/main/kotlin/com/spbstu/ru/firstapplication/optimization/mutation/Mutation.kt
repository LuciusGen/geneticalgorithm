package com.spbstu.ru.firstapplication.optimization.mutation

import com.spbstu.ru.firstapplication.optimization.Individual

interface Mutation {
    fun mutate(
        individual: Individual,
        limit: Double,
        value: Double,
        kj: List<Double>,
        zj: List<Double>
    ): Individual
}