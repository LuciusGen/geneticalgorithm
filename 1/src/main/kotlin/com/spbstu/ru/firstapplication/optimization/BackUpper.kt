package com.spbstu.ru.firstapplication.optimization

import kotlinx.coroutines.DelicateCoroutinesApi
import net.jafama.FastMath
import org.springframework.stereotype.Service
import java.io.File

@Service
@DelicateCoroutinesApi
class BackUpper {
    private val backUpFileName = "backUpFile.txt"
    private var iterationNumber = 1

    fun backUp(population: Population, backUpperFrequency: Int) {
        iterationNumber += 1
        if (FastMath.floorMod(iterationNumber, backUpperFrequency) != 0)
            return

        File(backUpFileName).printWriter().use { out ->
            out.println("iteration number $iterationNumber")

            out.println(population.toString())
        }
    }
}