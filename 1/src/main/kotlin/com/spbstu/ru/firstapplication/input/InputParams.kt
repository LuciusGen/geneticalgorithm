package com.spbstu.ru.firstapplication.input

data class InputParams(
    val cellFunctionName: String,
    val cellFunctionParamsNumber: Int,
    val limit: Double,
    val kj: Double,
    val zj: Double,
    val backUpIter: Int,
    val maxIter: Int
)