package com.spbstu.ru.firstapplication.optimization.function

import com.spbstu.ru.firstapplication.optimization.Individual
import java.io.InputStreamReader

class CellFunction(private val cellFunctionPath: String) {
    fun compute(params: Individual): Double {
        try {
            val process =
                Runtime.getRuntime()
                    .exec("$cellFunctionPath ${params.cellFunctionParams.joinToString(separator = " ")}")
            val reader = InputStreamReader(process.inputStream)

            return reader.readLines().first().toDouble()
        } catch (exception: Exception) {
            System.err.println("Can't compute cell function for params ${params.cellFunctionParams.joinToString(separator = " ")}")
            throw exception
        }
    }
}