package com.spbstu.ru.firstapplication.optimization.mutation

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration

@Configuration
class MutationStrategyConfiguration(
    private val defaultMutationStrategy: DefaultMutationStrategy = DefaultMutationStrategy()
) {
    private val mutationStrategies = HashMap<String, Mutation>(10)
    private val logger = LoggerFactory.getLogger(MutationStrategyConfiguration::class.java)

    init {
        mutationStrategies["default"] = defaultMutationStrategy
    }

    fun getMutationStrategy(strategyName: String): Mutation {
        val strategy = mutationStrategies.getOrDefault(strategyName, defaultMutationStrategy)
        logger.info("Create mutation strategy with name ${strategy::class.java}")
        return strategy
    }
}