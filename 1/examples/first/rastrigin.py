import sys
from math import pi, cos


def func(xx : list):
    sum = 0
    for x in xx:
        sum += x ** 2 - 10 * cos(2 * pi * x)
    return 10 * (len(xx)) + sum

xx = list()

for i in sys.argv[1:]:
    xx.append(float(i))

print(func(xx))
